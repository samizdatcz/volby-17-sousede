title: "Jakou barvu má váš okrsek? Prohlédněte si nejpodrobnější volební mapu"
perex: "Komu sebrali voliče Piráti? Kam se ztratili voliči ČSSD? A jak by dopadli Starostové, kdyby neutekli z koalice s lidovci?"
authors: ["Petr Kočí"]
published: "22. října 2017"
coverimg: "https://interaktivni.rozhlas.cz/data/volby-17-sousede/media/mapa-detail.png"
coverimg_note: "Foto <a href='https://www.lidovky.cz/komu-dali-hlas-vasi-sousede-prozkoumejte-mapu-s-nejpodrobnejsimi-volebnimi-daty-g1d-/zpravy-domov.aspx?c=A171020_215444_ln_domov_pev'>Lidovky.cz</a>"
styles: []
libraries: []
options: "" #wide
---

Odpovědi hledáme nad interaktivní mapou okrskových volebních výsledků, kterou [připravil server Lidovky.cz](https://www.lidovky.cz/komu-dali-hlas-vasi-sousede-prozkoumejte-mapu-s-nejpodrobnejsimi-volebnimi-daty-g1d-/zpravy-domov.aspx?c=A171020_215444_ln_domov_pev). O komentář k územnímu rozložení podpory pro všechny strany, jež se dostaly do Parlamentu, jsme požádali dva sociology, kteří se geografii výsledků parlamentních voleb věnují v [Sociologickém ústavu Akademie věd ČR](http://www.soc.cas.cz/oddeleni/lokalni-regionalni-studia).  

Zeměpisné trendy v chování českých voličů jsou podle nich dlouhodobě stabilní. "Území, kde jsou nadreprezentovány určité typy voličů, se dramaticky nemění. Mění se však to, kterou stranu si tyto typy voličů zvolí. Například ANO místo ČSSD, Piráty místo TOP09, SPD místo Věcí veřejných a republikánů...," říká sociální geograf Martin Šimon. "V podstatě je to posun od 'pravice a levice' k 'populistům a rasistům'."

<wide>
<iframe src="https://1gr.cz/o/lidovky/volby17/sousede/index.html" style="width: 100%; height: 70vh; border: none; margin-top:35px"></iframe>
</header> 
<div align="right" style="padding: 5px"><i>Interaktivní mapa: Marcel Šulek, <a href="http://lidovky.cz">Lidovky.cz</a></i></div>
</wide>
 
V interaktivní mapě můžete najetím myši zobrazit výsledky z každého okrsku. Přepínačem vlevo nahoře si pak můžete vybrat, jak se mapa vybarví: zda podle vítězné strany v každém okrsku, nebo jestli ukáže rozložení podpory pro vybranou partaj po celé republice.

Jednou z možností je také zobrazení volební účasti. Ta podle sociologa Josefa Bernarda vypadá podobně jako ve většině minulých voleb: "Projevuje se menší účast v pohraničí, v Karlovarském, Ústeckém, Plzeňském či Severomoravském kraji. Zjednodušeně můžeme říci, že jde o [bývalé Sudety](https://interaktivni.rozhlas.cz/sudety/). Je ovšem sporné, do jaké míry jde o přímou souvislost s výměnou obyvatelstva po druhé světové válce. Může to souviset také s tím, že to je území s dlouhodobou nezaměstnaností.""

## ANO: nevyhraněná tsunami

<img src="https://interaktivni.rozhlas.cz/data/volby-17-sousede/media/ano.png" width="100%"></img>

Hnutí Andreje Babiše zvítězilo nejen ve všech krajích, ale i ve všech okresech. Liší se jen velikost náskoku před soupeři. "Hlavní novinkou na letošní mapě je volební tsunami ANO. Jeho podpora je geograficky rozložená velmi rovnoměrně, snad jen s rozdílem menší podpory v Praze a okolí," říká Josef Bernard, vedoucí lokálních a regionálních studií Sociologického ústavu Akademie věd ČR.

"ANO má relativně rovnoměrné prostorové rozložení voličů. To je charakteristika 'nové' a populistické strany, která nemá stabilní voličské jádro," doplňuje ho jeho kolega Martin Šimon. "ANO je populistická strana typu 'catch-all' (všeuchopující, univerzální strana), proto je její volební výsledek prostorově plochý. Regionální rozdíly jsou vysvětlitelné spíše silnou přítomností jiných stran v regionech. Zjednodušeně lze říct, že nevyhraněná strana má nevyhraněný prostorový vzorec."

Praha a její okolí má pestřejší volební výsledek, což je podle Martina Šimona trend typický pro většinu evropských metropolí. "Hlavní město jakožto centrum státu s větší diverzitou a koncentrací funkcí má i diverzifikovanější elektorát."

## ODS: města a rekreační oblasti

<img src="https://interaktivni.rozhlas.cz/data/volby-17-sousede/media/ods.png" width="100%"></img>

Druhá nejsilnější Občanská demokratická strana bodovala v okolí velkých měst. "Mapa ukazuje tradiční obrázek: silnou podporu mívá ODS a další strany, které řadíme do kategorie fiskálně konzervativních, v širokém okolí Prahy, Brna, Liberce, Plzně či Českých Budějovic," říká sociolog Bernard.

Zvláštností ODS je podle něj úspěch v rekreačních oblastech, hlavně na Šumavě a v Krkonoších. Vysvětlení je dvojí: nízká hustota zalidnění a z ní vyplývající vyšší podíl podnikatelů, kteří v těchto oblastech provozují hotely a restaurace – a také Pražané, kteří do pohraničních hor odjíždějí o volebním víkendu za odpočinkem a hlasují tam s voličským průkazem.

## Piráti: od Náchoda po Domažlice

<img src="https://interaktivni.rozhlas.cz/data/volby-17-sousede/media/pirati.png" width="100%"></img>

Piráti letos poprvé zabodovali nejen ve městech, ale i ve venkovských regionech. "Mají prostorový vzorec podobný ODS a TOP 09 z minulých let: šikmý pás přes Čechy od Náchoda po Domažlice," všímá si Martin Šimon. "Toto území je v posledních letech ekonomicky úspěšné. Piráti proto získávají tradiční voliče ODS, kteří v minulých volbách zkusili TOP 09 se Starosty. Jedná se o spíše liberální voliče cílící na transparentnost. Piráti dosáhli zisku na venkově i ve městech, tím se liší od TOP 09 či Zelených."

Také Josef Bernard si všímá zvýšené podpory pro Piráty v Libereckém kraji či ve východních Čechách: "To jsou oblasti, které vždy volily pravicověji - v nich si teď Piráti velmi silně si konkurují s TOP 09 a ve východních Čechách s KDU-ČSL."

## SPD: Morava a severozápadní pohraničí

<img src="https://interaktivni.rozhlas.cz/data/volby-17-sousede/media/spd.png" width="100%"></img>

U SPD je jasně vidět dominance na Moravě a v severozápadním pohraničí. "To trochu kopíruje podporu ČSSD, jak jsme na ni byli zvyklí v předchozích volbách. Je na místě hypotéza, že SPD sociální demokracii sebrala velkou část elektorátu," říká Josef Bernard. "Čekal jsem, že výsledek SPD bude víc kopírovat historické výsledky pravicových extremistů, například DSSS, ale SPD se podařilo oslovit mnohem širší část voličstva."

"SPD – podobně jako v minulých volbách – mělo víc voličů v oblastech, kde jsou podle předloňské Gabalovy zprávy četnější sociálně vyloučené lokality. To znamená, že k volbě SPD motivují spíše místní sociální problémy než xenofobní mediální diskurz o uprchlících, kteří v Česku de facto žádní nejsou. V Čechách územní podpora SPD reflektuje periferní oblasti, kde jsou problémy se sociálním vyloučením a nezaměstnaností. SPD není silná v tradičních periferiích, kde jsou jiné typy problémů a lidé volí spíše KSČM," doplňuje svého kolegu Martin Šimon.

SPD mělo nejvyšší zisky na počátku sčítání hlasů, tedy v nejmenších obcích a na venkově obecně. "Argumenty přímé demokracie či xenofobní politiky rezonují tedy spíše mezi venkovskou populací než ve městech," říká Šimon.

## KSČM: tradiční bašty

<img src="https://interaktivni.rozhlas.cz/data/volby-17-sousede/media/kscm.png" width="100%"></img>

Komunisté od minulých voleb ztratili 350 tisíc hlasů, přesto zůstali silní ve svých tradičních baštách, jako je Bruntálsko či Jesenicko. "Obecně jde o oblasti s vyšší nezaměstnaností na chudším periferním venkově," říká Josef Bernard. Historické volební mapy ukazují v těchto oblastech komunistickou tradici sahající až do první republiky.

## ČSSD: staré jistoty už neplatí

<img src="https://interaktivni.rozhlas.cz/data/volby-17-sousede/media/cssd.png" width="100%"></img>

Sociální demokraté dosáhli nejhoršího výsledku od vzniku České republiky. Od posledních voleb ztratili přes 600 tisíc voličů. "ČSSD byla vždy v novodobých volbách strana s relativně nestabilní členskou základnou a proměnlivým prostorovým vzorcem," říká Martin Šimon. "Podle mého názoru globální pokles její podpory – převážně na úkor ANO – nemá výrazná prostorová specifika. ČSSD se na jednu stranu neprosadila v ekonomicky úspěšných regionech vůči pravicovým stranám, na druhou stranu ztratila v ekonomicky slabších regionech vůči xenofobním stranám."

V předchozích volbách komentátoři často mluvili o "oranžové Moravě", tato sociálnědemokratická jistota ale v letošních volbách padla. "V těchto volbách je Morava odlišná od Čech v daleko silnější podpoře SPD. To asi reflektuje horší ekonomický vývoj Moravy vůči Čechám. Voliči tak 'trestají' vládnoucí ČSSD za uplynulé období," domnívá se Šimon. Při sčítání hlasů podle okrsků nijak nerostl podíl ČSSD v čase, což naznačuje, že ČSSD ztratila pozice také ve větších městech.

## KDU-ČSL: fascinující stabilita

<img src="https://interaktivni.rozhlas.cz/data/volby-17-sousede/media/kdu.png" width="100%"></img>

Křesťanští demokraté se mohli i letos spolehnout na regiony s vysokým podílem věřících. "Lidovci jsou stranou s nejjasněji rozdělenou podporou, která silně souvisí s religiozitou," říká Josef Bernard.

"Územní rozložení volební podpory KDU-ČSL je fascinující, protože je stabilní již od první republiky. Nově si udržují zisky nad 5 procent i v Praze," doplňuje Martin Šimon. Souvisí to podle něj s migrací mladých lidí z venkova do hlavního města.

## TOP 09: pražská strana

<img src="https://interaktivni.rozhlas.cz/data/volby-17-sousede/media/top09.png" width="100%"></img>

TOP 09 je podle sociologů nejvíce městská ze všech stran – proto také průběžné výsledky sčítání až do poslední chvíle naznačovaly, že se do sněmovny nedostane. "Zachránily" ji až později sečtené hlasy z lidnatých pražských a středočeských okrsků.

Podle Josefa Bernarda je podpora TOP 09 úzce spojená se socioekonomickou úrovní a také s koncentrací městských, liberálních a proevropsky zaměřených voličů: "Tím lze vysvětlit její velmi vysokou podporu v Praze a v pražském zázemí." V dalších oblastech ji o voliče připravili zejména Piráti a STAN, kteří v minulých volbách kandidovali společně s TOP 09.

## Starostové: síla osobností

<img src="https://interaktivni.rozhlas.cz/data/volby-17-sousede/media/stan.png" width="100%"></img>

Podpora starostů úzce souvisí s popularitou a úspěchy jejích regionálních osobností, jako je starosta Kolína Vít Rakušan nebo Jan Farský, který zahájil politickou kariéru jako starosta v Semilech.

"Oproti minulým volbám, kde samostatně nekandidovali, je volební výsledek STAN novým prvkem: zcela jasně reflektuje sílu jejich lídrů na Semilsku, Kolínsku či Liberecku. U ostatních stran role osobností nejsou tak prostorově výrazné," říká Martin Šimon.

"Výsledek potvrzuje, že úvaha o předvolební koalici s KDU-ČSL byla na místě: starostové jsou silní tam, kde jsou lidovci slabí, a naopak. Kdyby vydrželi, nejen že by překonali hranici deseti procent pro vstup koalice do sněmovny, ale zároveň by byli silní ve velké části republiky, což o žádné z těchto stran samostatně neplatí," říká Josef Bernard.